const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  isPinned: { type: Boolean, default: false },
  isFavorite: { type: Boolean, default: false },
});

module.exports = mongoose.model('Todo', todoSchema);
// const express = require('express');
// const Todo = require('../models/todo');
// const authenticate = require('../middleware/authenticate');

// const router = express.Router();

// // Create To-Do
// router.post('/', authenticate, async (req, res) => {
//   const { title, description } = req.body;
//   const newTodo = new Todo({
//     userId: req.user.userId,
//     title,
//     description,
//   });
//   try {
//     await newTodo.save();
//     res.status(201).json(newTodo);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// // Get all To-Do items with pagination
// router.get('/', authenticate, async (req, res) => {
//   const { page = 1, limit = 10, search = '' } = req.query;
//   const query = { userId: req.user.userId, title: { $regex: search, $options: 'i' } };
//   try {
//     const todos = await Todo.find(query)
//       .skip((page - 1) * limit)
//       .limit(parseInt(limit));
//     res.status(200).json(todos);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// // Get single To-Do
// router.get('/:id', authenticate, async (req, res) => {
//   try {
//     const todo = await Todo.findOne({ _id: req.params.id, userId: req.user.userId });
//     if (!todo) return res.status(404).send('To-Do not found');
//     res.status(200).json(todo);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// // Update To-Do
// router.put('/:id', authenticate, async (req, res) => {
//   const { title, description, isPinned, isFavorite } = req.body;
//   try {
//     const todo = await Todo.findOneAndUpdate(
//       { _id: req.params.id, userId: req.user.userId },
//       { title, description, isPinned, isFavorite },
//       { new: true }
//     );
//     if (!todo) return res.status(404).send('To-Do not found');
//     res.status(200).json(todo);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// // Delete To-Do
// router.delete('/:id', authenticate, async (req, res) => {
//   try {
//     const todo = await Todo.findOneAndDelete({ _id: req.params.id, userId: req.user.userId });
//     if (!todo) return res.status(404).send('To-Do not found');
//     res.status(200).send('To-Do deleted');
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// // Tag To-Do as Pinned or Favorite
// router.patch('/:id/tag', authenticate, async (req, res) => {
//   const { isPinned, isFavorite } = req.body;
//   try {
//     const todo = await Todo.findOneAndUpdate(
//       { _id: req.params.id, userId: req.user.userId },
//       { isPinned, isFavorite },
//       { new: true }
//     );
//     if (!todo) return res.status(404).send('To-Do not found');
//     res.status(200).json(todo);
//   } catch (error) {
//     res.status(400).send(error);
//   }
// });

// module.exports = router;
