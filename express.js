const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Define a route for the root URL
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// Handle favicon.ico request to prevent 404
app.get('/favicon.ico', (req, res) => res.status(204));

// Listen for incoming requests
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
