
const express = require('express');
const app = express();
const port = 3000;

app.use(express.json()); // Middleware to parse JSON bodies

// Define a route for the root URL
app.get('/', (req, res) => {
    res.send('GET request to the TODO-LIST!!');
});
app.post('/', (req, res) => {
   res.send('POST request to the TODO-LIST!!');
 });
 app.put('/', (req, res) => {
   res.send('PUT request to the TODO-LIST!!');
 });
app.delete('/', (req, res) => {
  res.send('DELETE request to the TODO-LIST!!');
});
 

// Define other routes as needed
// For example, a route to get the list of TODO items
app.get('/todos', (req, res) => {
    res.json([{ id: 1, task: 'Do the dishes' }, { id: 2, task: 'Take out the trash' }]);
});
app.put('/todos', (req, res) => {
  res.json([{ id: 4, task: 'Do the dishes' }, { id: 3, task: 'Take out the trash' }]);
});

app.delete('/todos', (req, res) => {
  res.json([{ id: 5, task: 'Do the dishes' }, { id: 6, task: 'Take out the trash' }]);
});
// Define a route to handle POST requests to add a new TODO item
app.post('/todos', (req, res) => {
    const newTodo = req.body;
    // Normally, you would save the newTodo to a database here
    res.status(201).json(newTodo);
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});


