//const Todo = require('../models/Todo');

exports.createTodo = async (req, res) => {
  const { title, description } = req.body;
  const userId = req.user.userId;
  try {
    const newTodo = new Todo({
      userId,
      title,
      description,
    });
    await newTodo.save();
    res.status(201).send(newTodo);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

exports.getTodos = async (req, res) => {
  const userId = req.user.userId;
  const { page = 1, limit = 10, search = '' } = req.query;
  const query = {
    userId,
    title: { $regex: search, $options: 'i' },
  };
  try {
    const todos = await Todo.find(query)
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .exec();
    const count = await Todo.countDocuments(query);
    res.send({
      todos,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
    });
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.getTodo = async (req, res) => {
  const userId = req.user.userId;
  const { id } = req.params;
  try {
    const todo = await Todo.findOne({ userId, _id: id });
    if (!todo) {
      return res.status(404).send({ error: 'Todo not found' });
    }
    res.send(todo);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.updateTodo = async (req, res) => {
  const userId = req.user.userId;
  const { id } = req.params;
  const { title, description, isPinned, isFavorite } = req.body;
  try {
    const todo = await Todo.findOneAndUpdate(
      { userId, _id: id },
      { title, description, isPinned, isFavorite },
      { new: true }
    );
    if (!todo) {
      return res.status(404).send({ error: 'Todo not found' });
    }
    res.send(todo);
  } catch (error) {
    res.status (500).send({ error: error.message });
  }
};

exports.deleteTodo = async (req, res) => {
  const userId = req.user.userId;
  const { id } = req.params;
  try {
    const todo = await Todo.findOneAndDelete({ userId, _id: id });
    if (!todo) {
      return res.status(404).send({ error: 'Todo not found' });
    }
    res.send({ message: 'Todo deleted successfully' });
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};
